// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SH_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SPHEREHUNTER_API ASH_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ASH_PlayerController(const FObjectInitializer& ObjectInitializer);
	
};
