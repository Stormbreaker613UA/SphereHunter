// Copyright Epic Games, Inc. All Rights Reserved.

#include "SphereHunterHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "SH_W_InGameHud.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

ASphereHunterHUD::ASphereHunterHUD(const FObjectInitializer& ObjectInitializer) : Super (ObjectInitializer)
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	CrosshairTex = CrosshairTexObj.Object;
}


void ASphereHunterHUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition( (Center.X),
										   (Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem( CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem( TileItem );
}

void ASphereHunterHUD::BeginPlay()
{
	Super::BeginPlay();

	if (W_InGameHudClass != nullptr)
	{
		W_InGameHud = CreateWidget<USH_W_InGameHud>(GetWorld(), W_InGameHudClass);

		if (W_InGameHud)
		{
			W_InGameHud->AddToViewport();
		} 
	}

	
}

void ASphereHunterHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
