// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "SH_Sphere.generated.h"

UCLASS()
class SPHEREHUNTER_API ASH_Sphere : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASH_Sphere(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* SphereMeshComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USphereComponent* SphereCollisionComponent;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	class ASH_GameStateBase* GameState;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UAudioComponent* AudioComponent;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	USoundCue* POPAudioCue;
	
	UPROPERTY()
	float SizeScale = 1;

	// Called when the game starts or when spawned

	virtual void BeginPlay() override;

	virtual void Destroyed() override;
	

};
