// Copyright Epic Games, Inc. All Rights Reserved.

#include "SphereHunterGameMode.h"
#include "SphereHunterHUD.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"
#include "SphereHunterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

ASphereHunterGameMode::ASphereHunterGameMode(const FObjectInitializer& ObjectInitializer) : Super (ObjectInitializer)
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/BP_SH_PlayerCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASphereHunterHUD::StaticClass();
	SphereSpawnRadius = 1500;
	NumSpheresToSpawn = 10;
	IncreaseNumSpheresToSpawnProcent = 10;
	IncreaseRadiusProcent = 5;
}

void ASphereHunterGameMode::SpawnSpheres()
{
	ASHGameState->SetNumSpheresToNextWave(NumSpheresToSpawn);
	PlayerLocation = ASH_PlayerCharacter->GetActorLocation();
	
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	for (int i = 0; i < NumSpheresToSpawn; ++i)
	{
		
		FVector SpawnLocation = FVector(PlayerLocation.X + FMath::RandPointInCircle(SphereSpawnRadius).X, PlayerLocation.Y + FMath::RandPointInCircle(SphereSpawnRadius).Y, PlayerLocation.Z + FMath::RandRange(20.0f, 800.0f));
		
		auto SphereToSpawn = GetWorld()->SpawnActor<ASH_Sphere>(SphereClass,SpawnLocation,FRotator(0,0,0) ,SpawnInfo);
	};

	SphereSpawnRadius = SphereSpawnRadius + ( (SphereSpawnRadius * IncreaseRadiusProcent ) / 100.f ) ;
	NumSpheresToSpawn = NumSpheresToSpawn + ( (NumSpheresToSpawn * IncreaseNumSpheresToSpawnProcent) / 100 );
}

void ASphereHunterGameMode::BeginPlay()
{
	Super::BeginPlay();
	

	if(DefaultPawnClass->IsValidLowLevel())
	{
		ASH_PlayerCharacter = Cast<ASphereHunterCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	};

	if(GameState->IsValidLowLevel())
	{
		ASHGameState = Cast<ASH_GameStateBase>(GameState);
	};

	SpawnSpheres();
}

void ASphereHunterGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
