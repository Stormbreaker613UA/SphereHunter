// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SH_GameStateBase.generated.h"

class ASphereHunterGameMode;

UCLASS()
class SPHEREHUNTER_API ASH_GameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:

	ASH_GameStateBase(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintCallable)
	void UpdateScoreValue();

	UFUNCTION(BlueprintCallable)
    void UpdateWaveValue();

	UFUNCTION(BlueprintCallable)
	int GetScoreValue() const;

	UFUNCTION(BlueprintCallable)
    int GetWaveValue() const;
    
    UFUNCTION(BlueprintCallable)
	void SetNumSpheresToNextWave(int Number);

	UFUNCTION(BlueprintCallable)
    int GetNumSpheresToNextWave() const;
	
		
protected:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Score;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int NumSpheresToNextWave;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Wave;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	class ASphereHunterGameMode* ASHGameMode;
	
};
