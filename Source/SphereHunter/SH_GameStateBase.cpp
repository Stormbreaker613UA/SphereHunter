// Fill out your copyright notice in the Description page of Project Settings.


#include "SH_GameStateBase.h"
#include "SphereHunterGameMode.h"
#include "Kismet/GameplayStatics.h"

ASH_GameStateBase::ASH_GameStateBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	
	Score = 0.0f;
	Wave = 1.0f;
}

void ASH_GameStateBase::UpdateScoreValue()
{
	Score = Score + 1.0f;
	NumSpheresToNextWave = NumSpheresToNextWave - 1;
	if (ASHGameMode->IsValidLowLevel())
	{		
		if( NumSpheresToNextWave == 0)
			UpdateWaveValue();
	}
}

void ASH_GameStateBase::UpdateWaveValue()
{
	Wave = Wave + 1.0f;
	ASHGameMode->SpawnSpheres();
}

int ASH_GameStateBase::GetScoreValue() const
{
	return Score;
}

int ASH_GameStateBase::GetWaveValue() const
{
	return Wave;
}

void ASH_GameStateBase::SetNumSpheresToNextWave(int Number)
{
	NumSpheresToNextWave = Number;
}

int ASH_GameStateBase::GetNumSpheresToNextWave() const
{
	return NumSpheresToNextWave;
}

void ASH_GameStateBase::BeginPlay()
{
	Super::BeginPlay();

	if (GameModeClass->IsValidLowLevel())
	{
		ASHGameMode = Cast<ASphereHunterGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	}
}

void ASH_GameStateBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
