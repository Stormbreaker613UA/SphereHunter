// Fill out your copyright notice in the Description page of Project Settings.


#include "SH_Sphere.h"
#include "SH_GameStateBase.h"
#include "Components/AudioComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASH_Sphere::ASH_Sphere(const FObjectInitializer& ObjectInitializer) : Super (ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereCollisionComponent");
	SphereCollisionComponent->SetupAttachment(RootComponent);

	SphereMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("SphereMeshComponent");
	SphereMeshComponent->SetupAttachment(SphereCollisionComponent);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>("AudioComponent");
	AudioComponent->SetupAttachment(SphereCollisionComponent);
	AudioComponent->bAutoActivate = false;

	static ConstructorHelpers::FObjectFinder<USoundCue> POPCue(TEXT("'/Game/FirstPerson/Audio/POP.POP'"));
	POPAudioCue = POPCue.Object;

	AudioComponent->SetSound(POPAudioCue);
	
	
}

// Called when the game starts or when spawned
void ASH_Sphere::BeginPlay()
{
	Super::BeginPlay();

	if(GetWorld()->GetGameState()->IsValidLowLevel())
	{
		GameState = Cast<ASH_GameStateBase>(GetWorld()->GetGameState());
	}

	SizeScale = FMath::RandRange(0.25f, 2.0f);
	RootComponent->SetRelativeScale3D(FVector(SizeScale,SizeScale,SizeScale));

	SphereMeshComponent->SetCustomPrimitiveDataVector3(0,FVector(FMath::RandRange(0.0f,1.0f),FMath::RandRange(0.0f,1.0f),FMath::RandRange(0.0f,1.0f)));
}

// Called every frame
void ASH_Sphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASH_Sphere::Destroyed()
{
	if( IsValid(GameState) && (GameState != nullptr) )
		GameState->UpdateScoreValue();

	if(IsValid(POPAudioCue) && (POPAudioCue != nullptr))
		UGameplayStatics::PlaySoundAtLocation(this, POPAudioCue, GetActorLocation());
	
	Super::Destroyed();		
}


