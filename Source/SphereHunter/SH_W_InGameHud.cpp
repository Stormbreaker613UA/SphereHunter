// Fill out your copyright notice in the Description page of Project Settings.


#include "SH_W_InGameHud.h"
#include "SH_GameStateBase.h"

USH_W_InGameHud::USH_W_InGameHud(const FObjectInitializer& ObjectInitializer) : Super (ObjectInitializer)
{
	
}
void USH_W_InGameHud::NativeConstruct()
{
	Super::NativeConstruct();
}

void USH_W_InGameHud::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

void USH_W_InGameHud::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if(GetWorld()->GetGameState()->IsValidLowLevel())
	GameState = Cast<ASH_GameStateBase>(GetWorld()->GetGameState());
}