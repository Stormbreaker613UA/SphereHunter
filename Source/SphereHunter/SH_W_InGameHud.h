// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "SH_GameStateBase.h"
#include "SH_W_InGameHud.generated.h"

/**
 * 
 */
UCLASS()
class SPHEREHUNTER_API USH_W_InGameHud : public UUserWidget
{
	GENERATED_BODY()

public:
	USH_W_InGameHud(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (BindWidget))
	UTextBlock* WaveNumber;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (BindWidget))
	UTextBlock* ScoreNumber;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	class ASH_GameStateBase* GameState;
	
	// Optionally override the Blueprint "Event Construct" event
	virtual void NativeConstruct() override;

	// Optionally override the tick event
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	virtual void NativeOnInitialized() override;
	
};


