// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SH_W_InGameHud.h"
#include "SphereHunterHUD.generated.h"

UCLASS()
class ASphereHunterHUD : public AHUD
{
	GENERATED_BODY()

public:
	ASphereHunterHUD(const FObjectInitializer& ObjectInitializer);

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	class USH_W_InGameHud* W_InGameHud;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<USH_W_InGameHud> W_InGameHudClass;

	virtual  void BeginPlay() override;
    
    virtual void Tick(float DeltaSeconds) override;
	
protected:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

	

};

