// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SH_GameStateBase.h"
#include "SH_Sphere.h"
#include "SphereHunterCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "SphereHunterGameMode.generated.h"

UCLASS(minimalapi)
class ASphereHunterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASphereHunterGameMode(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float SphereSpawnRadius;
	
	UPROPERTY(BlueprintReadWrite,EditDefaultsOnly)
	float IncreaseRadiusProcent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int NumSpheresToSpawn;
		
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int IncreaseNumSpheresToSpawnProcent;

	UFUNCTION(BlueprintCallable)
    void SpawnSpheres();

protected:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	class ASphereHunterCharacter* ASH_PlayerCharacter;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	class ASH_GameStateBase* ASHGameState;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<ASH_Sphere> SphereClass;
	
	UPROPERTY(BlueprintReadWrite)
	FVector PlayerLocation;
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
};



